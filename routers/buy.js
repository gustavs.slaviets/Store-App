const express = require("express");
const router = express.Router();
const axios = require("axios");
const stripe = require("stripe")(process.env.STRIPE_KEY);
const getClient = require("../middleware/getClient");
const delete_item = require("../middleware/delete_item.js");
const fs = require("fs");

router.get("/", async (req, res) => {
  const id = req.query.id.split(",");
  if(id == undefined) { res.status(404).send("Item id not provided"); return; }

  let items = [];

  for(let i = 0; i < id.length; i++) {
    const item_id = id[i] * 1;
    const promise = await fetch(`${process.env.SITE_URL}/items/${item_id}`);
    const item = await promise.json();
    const images = item.image.map((img) => {
      return `${process.env.SITE_URL}/images/CMP-${img}`;
    });
    items.push({
      price_data: {
        currency: "eur",
        product_data: {
          name: item.title,
          images 
        },
        unit_amount: item.price * 100,
      },
      adjustable_quantity: {
        enabled: true,
        maximum: item.amount
      },
      quantity: 1
    });
  }
  const session = await stripe.checkout.sessions.create({
    success_url: `${process.env.SITE_URL}/buy/success?session_id={CHECKOUT_SESSION_ID}`,
    cancel_url: process.env.SITE_URL,
    phone_number_collection: {
      enabled: true,
    },
    line_items: items,
    metadata: {id: id.toString()},
    mode: 'payment'
  });
  res.redirect(await session.url);
});

router.get("/success", async (req, res) => {
  const session_id = req.query.session_id;
  if(session_id == undefined) {
    res.sendStatus(404);
    return;
  }
  const session = await stripe.checkout.sessions.retrieve(session_id);
  const line_items = await stripe.checkout.sessions.listLineItems(session_id);
  line_items.data.forEach(async (item, i) => {
    await delete_item(req, res, session.metadata.id[i], item.quantity)
  });
  res.render("thanks");
});

module.exports = router;
