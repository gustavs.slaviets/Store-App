const express = require("express");
const router = express.Router();
const getClient = require("../middleware/getClient");
const bcrypt = require("bcrypt"); //TODO: Maybe swithch to Argon2?
const sanitizer = require('sanitizer');

async function hashPassword(password) {
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    return hashedPassword;
}

async function checkPassword(password, hashedPassword) {
    const match = await bcrypt.compare(password, hashedPassword);

    return match;
}

router.get("/check/:name", async (req, res) => {
  const { name } = req.params;
  const client = await getClient();
  const exists = await client.query(`SELECT name FROM users WHERE LOWER(name)=LOWER('${sanitizer.sanitize(name)}');`);
  await client.end();
  res.json({ exists: (exists.rowCount > 0) });
});

router.post("/register", async (req, res) => {
  const { name, password } = req.body;
  const client = await getClient();
  const api_calls = 0;
  const api_key = "testkey";
  const max_api_calls = 200;

  const autologin = req.query.autologin;

  if([name, api_key, password, max_api_calls, api_calls].includes(undefined)) {
    res.send("undefined value provided");
    return;
  }

  //TODO: Make this more secure

  const api_key_hash = await hashPassword(api_key);
  const password_hash = await hashPassword(password);

  await client.query(`INSERT INTO users (name, api_key, password, max_api_calls, api_calls) VALUES (LOWER('${sanitizer.sanitize(name)}'), '${api_key_hash}', '${password_hash}', ${max_api_calls}, ${api_calls});`);
  const user = (await client.query(`SELECT privilege FROM users WHERE LOWER(name)=LOWER('${sanitizer.sanitize(name)}')`)).rows[0]; //Better to also query password if something fucks up
  await client.end();  
  if(autologin) {
    req.session.username = name;
    req.session.logged_in = true;
    req.session.privilege = user.privilege;
    res.redirect("/");
    return;
  }
  res.send("User inserted successfuly");
});

router.get("/logout", async (req, res) => {
  await req.session.destroy();
  res.redirect("/");
});

router.post("/login", async (req, res) => {
  const client = await getClient();
  const { name, password } = req.body;
  const redirect_url = req.query.redirect;
  if(password == undefined) { res.status(400).send("password not given"); return; }
  if(name == undefined) { res.status(400).send("username not given"); return; }
  
  const user = (await client.query(`SELECT password, privilege FROM users WHERE LOWER(name)=LOWER('${sanitizer.sanitize(name)}')`)).rows[0]; //TEMPORARY

  if(user == undefined) { res.status(404).redirect("/login?user_not_found"); return; }
  await client.end();

  const match = await checkPassword(password, user.password);
  if(match) {
    req.session.username = name;
    req.session.privilege = user.privilege;
    req.session.logged_in = true;
  } else {
    res.redirect("/login?password_incorrect");
    return;
  }
  if(redirect_url != undefined) {
    res.redirect(redirect_url);
    return;
  }
  res.json(req.session);
});

module.exports = router;
