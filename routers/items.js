const express = require("express");
const getClient = require("../middleware/getClient");
const router = express.Router();
const multer = require("multer");
const fs = require("fs");
const sharp = require("sharp");
const confirm_admin = require("../middleware/confirm_admin.js");
const delete_item = require("../middleware/delete_item.js");
const sizeOf = require("image-size");
const sanitizer = require('sanitizer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, process.env.IMAGE_DIR)
  },
  filename: function (req, file, cb) {
    const extArray = file.mimetype.split("/");
    const extension = extArray[extArray.length - 1];
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9) + "." + extension;
    cb(null, uniqueSuffix)
  }
})

const upload = multer({ storage })

router.get("/", async (req, res) => {
  const client = await getClient();
  const data = await client.query(`SELECT * FROM items;`);
  res.json(data.rows);
  await client.end();
});

router.get("/:id", async (req, res) => {
  const id = sanitizer.sanitize(req.params.id);
  const client = await getClient();
  if(!id) {res.json({error: "id undefined or null"}); return;}

  try {
    const data = await client.query(`SELECT * FROM items WHERE id=${id};`);
    res.json(data.rows[0]);
  } catch(err) {
    console.error(err);
    res.sendStatus(403);
  }
  await client.end();
});

router.get("/search/:query", async (req, res) => {
  const query = sanitizer.sanitize(req.params.query.replace(/_/g, " | "));
  const client = await getClient();
  if(query == undefined) {res.json({error: "query undefined"}); return;}
  const sql_query = `
    SELECT * FROM
      items,
      to_tsquery('${query}') query,
      NULLIF(ts_rank(to_tsvector(title), query), 0) rank_title,
      NULLIF(ts_rank(to_tsvector(description), query), 0) rank_description,
      SIMILARITY('${query}', title || description || immutable_array_to_string(tags)) similarity
    WHERE query @@ ts OR similarity > 0
    ORDER BY rank_title, rank_description, similarity DESC NULLS LAST;
  `;

  try {
    const data = await client.query(sql_query);
    res.json(data.rows);
  } catch(err) {
    console.error(err);
    res.sendStatus(403);
  }
  await client.end();
});

function string_to_tags(str) {
  let tags = "";
  if(str == "") return tags; 
  tags = [...new Set(str.split(" "))].map((tag) => {
    return `'${tag}'`
  }).join(",");
  return tags;
}

router.post("/", confirm_admin, upload.array("image"), async (req, res) => {
  const { title, description, price, discount, amount, thumbnail } = req.body;
  const image = req.files;

  const tags = string_to_tags(req.body.tags);

  if([title, description, price, discount, amount, tags].includes(undefined)) {
    res.status(406).json("Undefined values!");
    return;
  }

  const redirect_url = req.query.redirect;

  //Refactor asap
  image.forEach(async file => {
    const { path, filename } = file;
    const extArray = file.mimetype.split("/");
    const extension = extArray[extArray.length - 1];
    const file_dest = `${process.env.IMAGE_DIR}/CMP-${filename}`;
    const quality = 20;

    const size = sizeOf(path);
    const max_width = 1000;
    const max_height = 1000;
    if(size.width < max_width && size.height < max_height) return;
    const aspect = Math.max(size.width, size.height) / Math.min(size.width, size.height);
    
    let width, height;

    if(size.width > size.height) {
      width = max_width;
      height = max_height / aspect;
    } else {
      width = max_width / aspect;
      height = max_height;
    }

    await sharp(path)
    .resize(Math.round(width), Math.round(height))
    .withMetadata()
    .toFile(file_dest, (err, info) => {
      if(err) console.log(err);
      console.log(info);
    });
  });

  const image_list = image.map((img) => {
    return `'${img.filename}'`
  }).join(",");

  const client = await getClient();
  await client.query(
    `INSERT INTO items(title, description, price, discount, amount, image, tags, thumbnail_id) VALUES ('${title}', '${description}', ${price}, ${discount}, ${amount}, ARRAY[${image_list}], ARRAY[${tags}]::TEXT[], ${(thumbnail == undefined ? 0 : thumbnail)});`
  );
  await client.end();
  
  if(redirect_url != undefined) {
    res.redirect(redirect_url);
    return;
  }
  res.json("Data inserted!");
});

router.patch("/:id", confirm_admin, async (req, res) => {  
  const tags = string_to_tags(req.body.tags);

  const { title, description, price, discount, amount, id } = req.body;
  if([title, description, price, discount, amount, tags].includes(undefined)) {
    res.status(406).json("Undefined values!");
    return;
  }

  const client = await getClient();
  const thumbnail = (req.body.thumbnail == undefined ? 0 : req.body.thumbnail);
  const upd = await client.query(`UPDATE items SET title='${title}', description='${description}', price=${price}, discount=${discount}, amount=${amount}, tags=ARRAY[${tags}]::TEXT[], thumbnail_id=${thumbnail} WHERE id=${id};`);
  await client.end();
  res.sendStatus(200);
});

router.delete("/:id", confirm_admin, async (req, res) => {
  const client = await getClient();
  const amount = await client.query(`SELECT amount FROM items WHERE id=${req.params.id}`);
  if(amount == undefined) {
    res.status(404).send("Item not found");
    return;
  }
  await delete_item(req, res, client, req.params.id, amount.rows[0].amount)
  await client.end();
  res.sendStatus(200);
});

module.exports = router;
