# NodeJS Store App 

A fully functional web store built with [NodeJS](https://nodejs.org/en) with [Express.js](https://expressjs.com/)

## Features

- Modular product structure
- Clean backend API

## Developing

```
# clone repo
git clone https://github.com/gustavsDev/Store-App.git Store_App

# change directory to project
cd Store_App

# install all needed dependencies.
npm install
```

## Building

To run the store server, simply run `npm run server`
