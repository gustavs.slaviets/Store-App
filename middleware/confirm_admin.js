function confirm_admin(req, res, next) {
  if(req.session == undefined) {
    res.send("Session is undefined!");
    return;
  }
  if(req.session.privilege != 'admin') {
    res.status(401).send("Unauthorized");
    return;
  }
  next();
}

module.exports = confirm_admin;
