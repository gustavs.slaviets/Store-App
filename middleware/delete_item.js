const fs = require("fs");

async function delete_item(req, res, client, id, amount = 1) {
  if(id == undefined) {res.json({error: "id undefined"}); return;}

  const item_req = await client.query(`SELECT image, amount FROM items WHERE id=${id}`);
  if(item_req.rows.length <= 0) {
    res.send("item not found");
    return;
  }
  const item = item_req.rows[0];

  if(amount < item.amount) {
    await client.query(`UPDATE items SET amount=${item.amount - amount} WHERE id=${id}`);
  } else {
    const images = item.image;
    let image_paths = images.map(image => {
      return `${process.env.IMAGE_DIR}/${image}`;
    });
    images.map(image => {
      image_paths.push(`${process.env.IMAGE_DIR}/CMP-${image}`);
    });

    image_paths.forEach(path => {
      if(fs.existsSync(path)) {
        fs.unlinkSync(path);
        console.log(`${path} deleted`);
      } else {
        console.log(`Tried to delete ${path} but file doesn't exist!`);
      }
    });

    await client.query(`DELETE FROM items WHERE id=${id};`);
  }
}

module.exports = delete_item;
