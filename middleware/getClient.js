const postgres = require("pg");
const { Client, Pool } = postgres;

async function getClient(db) {
  const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT
  });
  const client = await pool.connect();
  return client;
} 

module.exports = getClient;
