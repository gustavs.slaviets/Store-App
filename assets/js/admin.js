const del_btns = Array.from(document.getElementsByClassName("delete-btn"));
const edit_btns = Array.from(document.getElementsByClassName("edit-btn"));

del_btns.forEach(btn => {
  btn.addEventListener("click", async () => {
    const id = btn.parentElement.getAttribute("data-item_id") * 1;
    if(!confirm("Are you sure?")) return;
    try {
      const res = await fetch(`/items/${id}`, {method: "DELETE"});
      if(res.ok) {
        location.href = "/";
      } else {
        alert("An error occured");
      }
    } catch(err) {
      console.log(err);
    }
  });
});

edit_btns.forEach(btn => {
  btn.addEventListener("click", async () => {
    const id = btn.parentElement.getAttribute("data-item_id") * 1;
    location.href = `/edit/${id}`;
  });
});
