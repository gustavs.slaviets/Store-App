let nav_toggled = false;

function update() {
  const is_desktop = window.innerWidth >= 600;
  if(!nav_toggled && !is_desktop) {
    document.getElementById("nav_ul").style.display = "none";
    return;
  }
  document.getElementById("nav_ul").style.display = (is_desktop ? "flex" : "block");
}

window.addEventListener('resize', update, true);

document.getElementsByClassName("mobile-dropdown")[0].addEventListener("click", () => {
  nav_toggled = !nav_toggled;
  update();
});

update();
