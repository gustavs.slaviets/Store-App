const images = Array.from(document.getElementsByClassName("item-image"));
let selected = 0;

document.getElementById("scroll-left").addEventListener("click", () => update(1));
document.getElementById("scroll-right").addEventListener("click", () => update(-1));

function update(delta) {
  selected += delta;
  if(selected < 0) selected = images.length - 1; // TODO: Update to use modulo
  if(selected > images.length - 1) selected = 0;
  images.forEach((image, i) => {
    image.style = `display: ${selected == i ? "block" : "none"}`;
  });
}

update(0);
