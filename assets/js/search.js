document.querySelector('#search-input').addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
      window.location.href = "/search/" + (document.querySelector("#search-input").value).replace(/ /g, "_");
    }
});
