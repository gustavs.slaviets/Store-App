const cart = document.getElementById("cart");

let cart_list = [];

async function addCartItem(id) {
  if(cart_list.includes(id)) return;
  cart_list.push(id);
  if(cart != null) {
    const item = await fetch(`/items/${id}`);
    const { image, thumbnail_id, title, price } = await item.json();
    const tr = document.createElement("tr");
    tr.dataset.item_id = id;
    tr.innerHTML = `
      <td><button class="cart-item-delete"><img width="30" height="30" src="https://img.icons8.com/FF0000/ios-glyphs/30/delete-sign.png" alt="delete-sign"/></button></td>
      <td><img src="/images/${image[thumbnail_id]}"></td>
      <td>${title}</td>
      <td class="cart_price" value="${price}">${price}€</td>
      <td width="70"><a href="/buy?id=${id}">Buy now</a></td>
    `;
    cart.appendChild(tr);
  }
  updateButtons();
}

function updateButtons() {
  const cart_delete_btns = document.getElementsByClassName("cart-item-delete");
  Array.from(cart_delete_btns).forEach(btn => btn.addEventListener("click", (e) => {
    const row = btn.parentNode.parentNode;
    const id = row.getAttribute("data-item_id");
    if(id == (null || undefined)) return;
    cart_list = cart_list.filter((item) => { return item != id; });
    row.remove();
    updateCart();
  }));
}

function updateSum() {
  const prices = Array.from(document.getElementsByClassName("cart_price")).map((e) => { return e.getAttribute("value") * 1; });
  prices.pop();
  document.getElementById("checkout-sum").innerHTML = prices.reduce((partialSum, a) => partialSum + a, 0);
}

function updateCart() {
  localStorage.setItem("cart", cart_list);
  const cart_count = localStorage.getItem("cart").split(",").length;
  if(cart != null) updateSum();
  if(localStorage.getItem("cart") == "") { document.getElementById("cart_count").style.display = "none"; return; }; 
  document.getElementById("cart_count").style.display = "flex"; 
  document.getElementById("cart_count").innerHTML = cart_count;
}

const cart_add_btn = document.getElementsByClassName("add-cart-btn");
Array.from(cart_add_btn).forEach(btn => btn.addEventListener("click", async (e) => {
  const id = btn.parentElement.getAttribute("data-item_id") * 1;
await addCartItem(id);
updateCart();
}));

if(cart != null) {
  document.getElementById("checkout_button").addEventListener("click", (e) => {
    if(cart_list.length <= 0) return;
    const ids = cart_list.join(",");
    location.href = `/buy?id=${ids}`;
  });
}

addEventListener("load", async function(e) {
  if(localStorage.getItem("cart") == ("" || null)) return;
  const ids = localStorage.getItem("cart").split(",");
  for(let i = 0; i < ids.length; i++) {
    const id = ids[i];
    if(id == "") return;
    await addCartItem(id * 1);
  }
  updateCart();
});
