document.addEventListener("load", (e) => {
  if(localStorage.getItem("scroll") == "") return;
  window.scrollTop(localStorage.getItem("scroll") * 1);
});

document.addEventListener("scroll", (e) => {
  localStorage.setItem("scroll", window.scrollY);
});
