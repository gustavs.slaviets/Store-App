const form = document.querySelector("form");
form.addEventListener("submit", async (e) => {
  e.preventDefault();
  const id = 1 * Array.from(window.location.pathname.split("/"))[2];
  const data = new URLSearchParams(new FormData(form));
  data.append("id", id);

  try {
    const res = await fetch(`/items/${id}`, {
      method: "PATCH",
      body: data
    });
    location.href = "/";
  } catch(err) {
    console.log(err);
  }
});
