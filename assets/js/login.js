const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const passwd_inc = document.getElementById("password_incorrect");
if(urlParams.has("password_incorrect")) { passwd_inc.innerHTML = "Password is incorrect"; passwd_inc.style.display = "block"; }
if(urlParams.has("user_not_found")) {passwd_inc.innerHTML = "User not found"; passwd_inc.style.display = "block"; }
