const tags_el = document.getElementById("tags");
const tag_preview = document.getElementById("tag_preview");
function update() {
  let el = "";
  if(tags_el.value == "") {
    tag_preview.innerHTML = "";
    return;
  }
  tags_el.value.split(" ").forEach(t => {
    el += `<p class="tag">${t}</p>`;
  });
  tag_preview.innerHTML = el;
}
tags_el.addEventListener("input", update);
addEventListener("load", update);
