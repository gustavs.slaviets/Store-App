const chooseFile = document.getElementById("image");
const imgPreview = document.getElementById("img-preview");

chooseFile.addEventListener("change", function () {
  imgPreview.innerHTML = "";
  for(let i = 0; i < chooseFile.files.length; i++) {
    const div = document.createElement("div");
    const image_div = document.createElement("div");
    image_div.appendChild(getImgData(i));
    const radio = document.createElement("input");
    radio.type = "radio";
    radio.name = "thumbnail";
    radio.value = i;
    div.appendChild(radio);
    div.appendChild(image_div);
    div.className = "image";
    imgPreview.appendChild(div);
  }
});

function getImgData(id = 0) {
  const files = chooseFile.files[id];
  let elem = document.createElement("img");
  if (files) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(files);
    fileReader.addEventListener("load", function () {
      elem.setAttribute("src", this.result);
    });    
  }
  console.log(elem);
  return elem;
}
