document.querySelector("form").addEventListener("submit", async (e) => {
  e.preventDefault();

  if(document.getElementById("password").value != document.getElementById("check_password").value) {
    document.getElementById("password_incorrect").innerHTML = "Password doesn't match!";
    return;
  };  

  const res = await fetch(`/user/check/${document.getElementById("name").value}`);
  const { exists } = await res.json();
  if(exists) {
    document.getElementById("password_incorrect").innerHTML = "User already exists!"
    return; 
  }
  HTMLFormElement.prototype.submit.call(document.querySelector("form"));
});
