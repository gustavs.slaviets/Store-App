require("dotenv").config();
const cors = require("cors");
const crypto = require("crypto");
const express = require("express");
const fs = require("fs");
const morgan = require("morgan");
const sessions = require('express-session');
const ejs = require("ejs");
const axios = require("axios")
const helmet = require("helmet");

const PORT = process.env.PORT;

const app = express();

/* ===================== MIDDLEWARE ============================ */

app.use(
  helmet({
    contentSecurityPolicy: {
      directives: {
        "script-src": ["'self'", "veclietas.lv"],
        "style-src": ["'self'", "veclietas.lv", "fonts.googleapis.com", "cdnjs.cloudflare.com"],
        "img-src": ["data:", "'self'", "veclietas.lv", "picsum.photos", "img.icons8.com", "fastly.picsum.photos"],
      },
    },
  }),
);

app.use(
  sessions({
    secret: crypto.randomBytes(48).toString('base64url'),
    cookie: {
      maxAge: 1000 * 60 * 60 * 24, // 24 hours
    },
    resave: true,
    saveUninitialized: false,
  })
);

app.use(morgan(":method :status :url"));
app.use(cors());
app.use(express.urlencoded({ extended: true, limit:'999mb', parameterLimit:1000000 }));
app.use(express.json());
const getClient = require("./middleware/getClient"); //Postgres client
const confirm_admin = require("./middleware/confirm_admin.js");

app.set("view engine", "ejs");

/* ===================== ROUTERS =============================== */
(async () => {
  const client = await getClient();
  console.log("Database init.");
  await client.query(fs.readFileSync("sql/CREATE_DB.sql", "utf8"));
  await client.query(fs.readFileSync("sql/TEXT_SEARCH.sql", "utf8"));
  await client.end();
})();

app.use("/items", require("./routers/items"));
app.use("/buy", require("./routers/buy"));
app.use("/user", require("./routers/users"));

app.get("/", async (req, res) => {
  const items = await axios.get(`${process.env.SITE_URL}/items`);
  res.render("index", {
    title: "Products",
    session: req.session,
    items: items.data
  });
});

app.get("/post", confirm_admin, (req, res) => res.render("post"));
app.get("/edit/:id", confirm_admin, async (req, res) => {
  const item = (await axios.get(`${process.env.SITE_URL}/items/${req.params.id}`)).data;
  res.render("edit", { item });
});

app.get("/login", (req, res) => res.render("login"));
app.get("/register", (req, res) => res.render("register"));

app.get("/cart", (req, res) => res.render("cart", { session: req.session }));
app.get("/view", async (req, res) => {
  const id = req.query.id;
  if(id == undefined) {
    res.send("404 Item not found");
    return;
  }
  const item = (await axios.get(`${process.env.SITE_URL}/items/${id}`)).data;
  res.render("view", {
    id,
    category: item.category,
    tags: (item.tags == (null || undefined) ? [] : item.tags),
    images: item.image,
    title: item.title,
    price: item.price,
    description: item.description,
    amount: item.amount
  });
});

app.get("/search/:query", async (req, res) => {
  const query = req.params.query;
  const items = await axios.get(`${process.env.SITE_URL}/items/search/${query}`);
  res.render("index", {
    title: "Search results",
    session: req.session,
    items: items.data
  });
});

function sendFile(res, dir) {
  const file = process.cwd() + dir;
  if(fs.existsSync(file))
    res.sendFile(file);
  else
    res.sendFile(process.cwd() + "/assets/NOT_FOUND.png");
}

app.get("/images/:image", (req, res) => {
  const file = process.env.IMAGE_DIR + `/${req.params.image}`;
  if(fs.existsSync(file))
    res.sendFile(file);
  else
    res.sendFile(process.cwd() + "/assets/NOT_FOUND.png");
});
app.get("/css/:url", (req, res) => sendFile(res, `/assets/css/${req.params.url}`));
app.get("/js/:url", (req, res) => sendFile(res, `/assets/js/${req.params.url}`));
app.get("/assets/:url", (req, res) => sendFile(res, `/assets/${req.params.url}`));

app.listen(PORT, () => console.log(`Port ${PORT} open.`));
