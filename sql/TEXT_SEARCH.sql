CREATE OR REPLACE FUNCTION immutable_array_to_string(text[]) RETURNS text as $$ SELECT array_to_string($1, ' '); $$ LANGUAGE sql IMMUTABLE;

ALTER TABLE items ADD COLUMN IF NOT EXISTS ts tsvector
GENERATED ALWAYS AS
 (setweight(to_tsvector('simple', coalesce(title, '')), 'A') ||
 setweight(to_tsvector('simple', coalesce(description, '')), 'B') ||
 setweight(to_tsvector('simple', immutable_array_to_string(tags)), 'C')) STORED;
CREATE INDEX IF NOT EXISTS ts_idx ON items USING GIN (ts);
