CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE IF NOT EXISTS items(
  id SERIAL PRIMARY KEY,
  title TEXT,
  description TEXT,
  tags text [],
  price REAL DEFAULT 0.5,
  discount INT DEFAULT 0,
  amount INT DEFAULT 1,
  thumbnail_id INT DEFAULT 0,
  image TEXT [] 
);

DO $$ BEGIN
  CREATE TYPE user_privilege AS ENUM ('user', 'moderator', 'admin');
EXCEPTION
  WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS users(
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) UNIQUE,
  privilege user_privilege DEFAULT 'user', 
  api_key VARCHAR(255),
  password VARCHAR(255),
  max_api_calls INT DEFAULT 3600,
  api_calls INT DEFAULT 0
);
